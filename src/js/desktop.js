jQuery.noConflict();
(function ($, PLUGIN_ID) {
  "use strict";
  kintone.events.on('app.record.index.show', function () {

    if ($('.kintone-app-headermenu-space').has("button").length) {
      $('.kintone-app-headermenu-space').children("button").remove();
    }
    $('.kintone-app-headermenu-space').append(`
      <button type="button" class="btn btn-primary" onclick="getChart()">get chart</button>
    `);

    $('.box-gaia').append(`
      <div class="chart-view">
        <canvas id="canvas" width="800" height="300" aria-label="Chart"></canvas>
      </div>
    `);

  });



})(jQuery, kintone.$PLUGIN_ID);

async function getRecords() {
  const proxyurl = "https://cors.pirago.vn/";
  // https://btmc.vn/thong-tin/tai-lieu-api/api-gia-vang-17784.html
  const url = 'http://api.btmc.vn/api/BTMCAPI/getpricebtmc?key=3kd8ub1llcg9t45hnoh8hmn7t5kc2v';
  return await new Promise((resolve, reject) => {
    fetch(proxyurl + url, {
      method: 'GET',
      headers: new Headers({
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/x-www-form-urlencoded"
      }),
    })
      .then(response => response.text())
      .then(data => {
        resolve(JSON.parse(data).DataList.Data);
      }).catch(console.error);
  });
}

async function getChart() {
  const data = await getRecords();
  var chartData = {};
  var labels = [];
  var purchasePrice = [];
  var priceData = [];
  var priceT = [];
  for (let i = 0; i < data.length; i++) {
    labels.push(data[i]['@n_' + data[i]['@row']]);
    purchasePrice.push(data[i]['@pb_' + data[i]['@row']]);
    priceData.push(data[i]['@ps_' + data[i]['@row']]);
    priceT.push(data[i]['@pt_' + data[i]['@row']]);
  }
  chartData = {
    'labels': labels,
    datasets: [
      {
        label: 'Giá mua vào',
        data: purchasePrice,
        stack: 'combined',
        backgroundColor: 'red',
        type: 'bar'
      },
      {
        label: 'Giá Bán ra',
        data: priceData,
        backgroundColor: 'blue',
        borderColor: 'blue',
        stack: 'combined'
      },
      {
        label: 'Giá thế giới',
        data: priceT,
        backgroundColor: 'green',
        borderColor: 'green',
        stack: 'combined'
      }
    ]
  };
  createChart(chartData);
}

function createChart(data) {
  let ctx = document.getElementById('canvas');
  chart = new Chart(ctx, {
    type: 'line',
    data: data,
    options: {
      plugins: {
        title: {
          display: true,
          text: 'Gold Price'
        }
      },
      scales: {
        y: {
          stacked: true
        }
      }
    },
  });
}